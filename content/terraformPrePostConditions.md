---
title: "Terraform Pre Conditi.... <Error Char limit exceeded>"
date: "2022-05-6"
author: "Jake Kudiersky"
cover: "static/img/GitlabRunner.png"

---

#### An Intro to Terraform Pre and Post Conditions ####

Every organisation I have worked with, has a different naming convention for their resources. For better or worse, they are all created with the intention of being as informative as possible, which maintaining some resemblance of a consistent structure.

As many of you may be aware in AWS, our resource names are limited, in most cases this would bot be an issue. For example you would be hard pressed to create an SNS topic that is 256 characters.

An example for an SQS Name 

`MyOrganisation-MyAWSAccount-MyProject-MyQueueName-MyEnvironment`
Arguably the most important part of the resource name is `MyQueueName`. But limit to 80 Characters, I have often seen names being put on a quick diet; `ImportantEvent` being squeezed to `ImtEvnt` to fit within your constraints.


Terraform has many wonderful features. Custom Variable Validation were added in Terraform 0.13. Allowing us to validate our variables.

```
variable "ami_id" {
    type = string

    validation {
    condition = (
        length(var.ami_id) > 4 
    )
    error_message = "The ami_id value must be greater than 4."
    }
}
```
However, we do not often use a variable in its entirety for a resource name, we often add a `terraform.workspace` or `var.environment` suffix or a prefix.


### Version 1.2.0 

Checking the Release notes of Release Candidate 1.2.0, we can see a welcome addition of Pre and Post Conditional check blocks, that we could now use with our resources.

[1.2.0] https://github.com/hashicorp/terraform/releases/tag/v1.2.0-rc1

# Show Me..

Lets take a SQS name using the `aws_sqs_queue` resource. The name of the queue. Queue names must be made up of only uppercase and lowercase ASCII letters, numbers, underscores, and hyphens, and must be between 1 and 80 characters long.

precondition — expectation or a guess about some external (but within a module) value that a resource depends on.


The special self object is available only for the postcondition block because it assumes that validation can be performed after the object is created and its attributes are known.

lambda name character limit

```
resource "random_string" "random80" {
  length = 80
  special = false
}

resource "random_string" "random85" {
  length = 85
  special = false
}

resource "aws_sqs_queue" "queue" {
  name                      = random_string.random80.result
#   lifecycle {
#       postcondition {
#           condition = length(self.name) > 100000
#           error_message = "nah mate"
#       }
#   }
}
```


## References

[Terarform-EKS](https://registry.terraform.io/providers/)
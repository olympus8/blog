---
title: "Gitlab Runner with a Nautical Theme"
date: "2022-05-6"
author: "Jake Kudiersky"
cover: "static/img/GitlabRunner.png"

---

### A Gitlab Runner for Kubernetes

**Iterate Iterate Iterate**, Gitlab’s latest press pack reminds us of the mantra that we Engineers thrive on: learning, improving and delivering value. Making our code DRYer, our pipelines faster, increasing visibility.  We need a tool to make our lives easier; Gitlab CI/CD is a key DevOps suite that allows us to ship code faster and deploy to any environment.
The Gitlab Runner is an application that works with Gitlab CI/CD to run jobs in a pipeline, using (often) simple YAML configuration that any developer can understand.

### Iterating - How we use the Gitlab Runner
The first experience for many using Gitlab CI, is to use a shared runner. Great to get up and running, but as our pipelines grow, our security requirements tighten and we exceed our free 400 minutes, many of us would look to hosting our own runners. This of course is an option that allows us more control, but with the added management overhead of an instance. 

A key driver for adopting Kubernetes is the improved management, scaling and networking of containers, but how would we host a Gitlab Runner on Kubernetes? This article will show you how to run your Gitlab Runner like any other micro service deployed to your cluster.

> We will be utilising Terraform, Helm and EKS throughout this article, but the principles will remain the same for which ever cloud provider or deployment tool is your weapon of choice 

Some Key topics we will cover:
  - Helm deployment with Terraform
  - Service Account creation
  - Tagging the pipeline

The `Kubernetes Executor`, when used with GitLab CI, connects to the Kubernetes API in the cluster creating a pod for each GitLab CI Job.

### How will this look ?

Our Kubernetes Executor will be deployed to a EKS cluster, in a `gitlab-runner` namespace. The pods will be created in the same namespace. My AWS account is `olympus` and my EKS Cluster is `olympus`.

{{< image src="/blog/static/img/executorDiagram.png" alt="diagram" position="center" style="border-radius: 8px;" >}}

### How will this work ?
{{< image src="/blog/static/img/flowDiagram.png" alt="diagram" position="center" style="border-radius: 8px;" >}}


### Need a cluster ?

(If you already have a cluster you can move on)
For this demo I am using an EKS cluster provisioned with eksctl.

> `eksctl` is a tool I've often used to help provision an EKS cluster in minutes. However, I would advise exploring other options for deploying a Production cluster, such as Terraform. 

To make life a little simpler we are using `AWS Fargate` to manage the infrastructure for our pods.

```bash
$ eksctl create cluster --fargate --name olympus
[ℹ]  eksctl version 0.11.0
[ℹ]  using region eu-west-2
[ℹ]  kubectl command should work with "/Users/jakekudiersky/.kube/config", try 'kubectl get nodes'
[✔]  EKS cluster "olympus" in "eu-west-2" region is ready
```

To allow us to provision pods in Fargate for EKS we must add a `Fargate Profile`. A name space is required, we will use `gitlab-runner`.

```bash
$ kubectl create ns gitlab-runner
```
```bash
$ eksctl create fargateprofile \
    --cluster olympus \
    --name gitlab-runner \
    --namespace gitlab-runner 
```

### Validate our cluster

```bash
$ kubectl get pods -A
NAME                            READY   STATUS    RESTARTS   AGE
kube-system     coredns-787d49646-9m9zp         1/1     Running   0          30s
kube-system     coredns-787d49646-mpwld         1/1     Running   0          30s
```

### Registering the runner

Gitlab allows us to register a runner; it binds the runner with all projects within the group. When registering at the group level we are provided with a key, this is a sensitive value and as such will be stored in `Param Store`. The values can then be referenced via Terraform using a `Data Source`.

{{< image src="/blog/static/img/groupRunner.png" alt="group-runner" position="center" style="border-radius: 8px;" >}}

To store the value in `Param Store`, we can use the `AWS CLI`, or manually create the resource in the account associated with our cluster.

```bash
aws ssm put-parameter \
    --name "OLYMPUS_GROUP_GLR_TOKEN" \
    --value XXXXXXXXXXXXXXXXXXXXXXX \
    --type "SecureString"
```

## Deploying the Kubernetes Executor with Terraform

> All the Terraform code is available in this [Repo](https://gitlab.com/olympus8/kubernetes-gitlab-runner)

`Helm.tf` - This will deploy the publicly available chart for the Kubernetes Executor
`iam.tf` - Includes the Service Account and permissions to fetch the secure values from `Param Store`

### tfvars

For this demo I will be using `Olympus` as my environment name. This will also act as a conditional for the account in which I am deploying the Kubernetes Executor `olympus.auto.tvars`.


```go
environment = "olympus"
excecutor_account_id = <my_account_id>
```

> `backend.tf` It is good practice to use a remote state file when using terraform, this example uses s3. You can remove this file to use a local state file


### helm_release Terraform resource

Helm simplifies the management of applications on Kubernetes, so it is no surprise Terraform has a resource to manage this. This can also be deployed via the Helm cli [helm_release resource](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release).

```go
resource "helm_release" "gitlab_runner" {
  count            = var.environment == "olympus" ? 1 : 0
  name             = local.runner_name
  namespace        = local.runner_name
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-runner"
  version          = "0.38.1"

  values = [
    "${file("${path.module}/values.yaml")}"
  ]

  set {
    name  = "gitlabUrl"
    value = "https://gitlab.com/"
  }

...

```

**Sets** - Value block with custom values to be merged with the values yaml

**Values** - List of values in raw yaml to pass to Helm. Values will be merged, in order, as Helm does with multiple -f options

### The Chart 

Gitlab maintain a public chart for the Kubernets Executor. It is a useful exercise to view the `values.yaml` file to see the configuration items. 

[Chart](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/values.yaml)


### OIDC Connector

We will also pass the Cluster OIDC Idenity Issuer to `Param Store`. This will be used within the `Service Account` IAM resource.

We will first use the `AWS CLI` to get the identity issuer url and trim the `https://` prefix.

```bash 
export ISSUER=$(aws eks describe-cluster \
--name olympus --query "cluster.identity.oidc.issuer" \
--output text | grep -oP "https://\K.*")
```
```bash
aws ssm put-parameter \
    --name "OLYMPUS_OIDC_PROVIDER" \
    --value ${ISSUER} \
```

## Service Account

You can associate an IAM role with a Kubernetes Service Account. This service account can then provide AWS permissions to the containers in any pod that uses that service account.

As we are using a `Service Account` to allow our pods to access AWS resources, we make use of the following value
```
  set {
    name  = "rbac.serviceAccountAnnotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.gitlab_runner_service_account[count.index].arn
  }
``` 

The `Service Account` is key to allowing our Kubernetes Executor to fetch the `Gitlab Token` from `Param Store`.

### IAM Roles


We have created a specific role for the Job Pods. The Service Account is given an `assume role policy` with a federated principal of the OIDC provider ARN, to allow `AssumeRoleWithWebIdentity`.


```go
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${data.aws_ssm_parameter.eks_oidc_token_services[count.index].value}:aud"
      values   = ["sts.amazonaws.com"]
    }

    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.id}:oidc-provider/${data.aws_ssm_parameter.eks_oidc_token_services[count.index].value}"]
      type        = "Federated"
    }
  }
```

## Running Terraform

To deploy the resources required, run from the root of the project. I am using Terraform `v1.1.9`.

```go
terarform init
terraform apply
-y
```
We can then validate our Helm deployment
```
helm_release.gitlab_runner[0]: Still creating... [1m10s elapsed]
helm_release.gitlab_runner[0]: Still creating... [1m20s elapsed]
helm_release.gitlab_runner[0]: Still creating... [1m30s elapsed]
helm_release.gitlab_runner[0]: Creation complete after 1m32s [id=gitlab-runner]

Apply complete! Resources: 7 added, 0 changed, 0 destroyed.

```

```go
$ kubectl get pods -A              
NAMESPACE       NAME                            READY   STATUS    RESTARTS   AGE
gitlab-runner   gitlab-runner-c8bdf75f7-fx86p   1/1     Running   0          4m20s
gitlab-runner   gitlab-runner-c8bdf75f7-nv2cn   1/1     Running   0          4m20s
gitlab-runner   gitlab-runner-c8bdf75f7-vt4vj   1/1     Running   0          4m20s
```

> The number of executors running is defined within the `values.yaml` file

## Tags

In the `values.yaml` we declare the tags. Theses tags are then required in the `.gitlab-ci.yml` to be picked up by the runners.
```yaml
runners:
  config: |
    [[runners]]
      [runners.kubernetes]
        image = "alpine:latest"
  tags: "kubernetes"
```

Once the runner is registered we can see which tags it wants.

{{< image src="/blog/static/img/registerRunner.png" alt="group-runner" position="center" style="border-radius: 8px;" >}}

## .gitlab-ci yml 

The most DevOps-way I could think of to display a pipeline, was to use `Hugo`.
Hugo is a fast and modern static site generator written in Go, that comes with some great [documentation](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/). This pipeline uses a custom image that allows a `hugo` script to run, that will compile the assets and upload them to GitLab Pages, where this site is hosted. We have added the `kubernetes` tag to allow the Group runner to pick it up.

```yaml
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  tags:
    - kubernetes
```

> If you find your jobs are stuck in a pending state, this is often due to the job not having the correct tags and therefore cannot be picked up by a runner


### Let's see it in action

Now all the pieces are in place we can make a simple change to our code.

We can see our executors running
```
➜ k get pods -n gitlab-runner    
NAME                            READY   STATUS    RESTARTS   AGE
gitlab-runner-897c5d877-9m8nq   1/1     Running   0          8d
gitlab-runner-897c5d877-c2gn9   1/1     Running   0          8d
gitlab-runner-897c5d877-xsc88   1/1     Running   0          8d

```

We make a change in the code and commit it to our default branch and see the Job output.


```go
Running on runner-yen9yvdz-project-35871312-concurrent-0zvz2z via gitlab-runner-c8bdf75f7-nv2cn...
Getting source from Git repository
00:04
Fetching changes with git depth set to 20...
Initialized empty Git repository in /builds/olympus8/blog/.git/
Created fresh repository.
Checking out 4f5d07dc as master...
Updating/initializing submodules recursively with git depth set to 20...
Submodule 'themes/terminal' (https://github.com/panr/hugo-theme-terminal.git) registered for path 'themes/terminal'
Cloning into '/builds/olympus8/blog/themes/terminal'...
Submodule path 'themes/terminal': checked out 'e0213b0d4eaa399770a11be1faff89529b2ced0b'
Entering 'themes/terminal'
Entering 'themes/terminal'
Executing "step_script" stage of the job script
00:01
$ hugo
Start building sites … 
hugo v0.98.0-165d299cde259c8b801abadc6d3405a229e449f6+extended linux/amd64 BuildDate=2022-04-28T10:23:30Z VendorInfo=gohugoio
                   | EN  
-------------------+-----
  Pages            | 10  
  Paginator pages  |  0  
  Non-page files   |  6  
  Static files     | 15  
  Processed images |  0  
  Aliases          |  1  
  Sitemaps         |  1  
  Cleaned          |  0  
Total in 64 ms
Uploading artifacts for successful job
00:02
Uploading artifacts...
public: found 46 matching files and directories    
Uploading artifacts as "archive" to coordinator... 201 Created  id=2443718705 responseStatus=201 Created token=WztZ3WNK
Cleaning up project directory and file based variables
00:00
Job succeeded
```

Here we see the created pod along side the executor. The pod has 2 containers. The `builder` and the `helper`.

```go
gitlab-runner-c8bdf75f7-fx86p                        1/1     Running   0          8m28s
gitlab-runner-c8bdf75f7-nv2cn                        1/1     Running   0          8m28s
gitlab-runner-c8bdf75f7-vt4vj                        1/1     Running   0          8m28s
runner-yen9yvdz-project-35871312-concurrent-0zvz2z   2/2     Running   0          53s

```

```bash
 kubectl get pod runner-nltgzbx-project-23855912-concurrent-0f8cxr -n gitlab-runner -o jsonpath='{.spec.containers[*].name}'
 helper
 builder

```

These pods will run for the entirety of the Job and be terminated by the gitlab-runner pods.

### Summary

Gitlab's Kubernetes Executor allows us to make use of all the great features of a self-hosted runner, whilst leveraging the benefits of container orchestration through Kubernetes. We brought a service account into the mix which allowed us to use sensitive values stored in Param Store. We deployed all this with Terraform to the cluster we provisioned with eksctl, all in a few minutes.

We created a gitlab-ci.yml file with the tags needed for the Kubernetes Executor to pick up the jobs and viewed the pods being created in the cluster.

### Until next time
Look forward to my next post, where we tailor the solution to allow us to create an Infrastructure as Code pipeline using AWS IAM Cross Account Roles and a custom Container Image.

I hope this post can inspire you to share where you are on you Kubernetes journey. Any questions, get in touch [Linkdin](https://www.linkedin.com/in/jakekudiersky/).

## References

[Fargate](https://docs.aws.amazon.com/eks/latest/userguide/fargate.html)
[Terarform-EKS](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster)
[eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)
[gitlab-runner](https://docs.gitlab.com/runner/)
[install-gitlab-runner-k8s](https://docs.gitlab.com/runner/install/kubernetes.html)
[install-gitlab-excutor](https://docs.gitlab.com/runner/executors/kubernetes.html)


---
title: "Gitlab Runner with a Nautical Theme"
date: "2022-05-6"
author: "Jake Kudiersky"
cover: "static/img/tfgitlabrunner.png"

---

#### A Gitlab Runner for Kubernetes ####

In a previous article we discusses how to deploy the Gitlab Runner ([Deploying Gitlab Runner to Kubernetes](file:///C:/my_file.pdf)). This article will build on this, making use of the Gitlab Runner Executor to create Pods with access in associated AWS Account via Cross Account Roles. 

We will display this through a Terraform Pipeline, giving you an insight into how you can create an Infrastructure as Code pipeline using the Gitlab Runner Executor deployed to EKS and run Jobs for each Environment and Job.


### OIDC Connector

We will also pass the Cluster OIDC Idenity Issuer to `Param Store`. This will be used within the `Service Account` IAM resource.

We will first use the `AWS CLI` to get the identity issuer url and trim the `https://` prefix.

```bash 
export ISSUER=$(aws eks describe-cluster \
--name olympus --query "cluster.identity.oidc.issuer" \
--output text | grep -oP "https://\K.*")
```
```bash
aws ssm put-parameter \
    --name "OLYMPUS_OIDC_PROVIDER" \
    --value ${ISSUER} \
```

## Service Account

With service roles, you can associate an IAM role with a Kubernetes service account. This service account can then provide AWS permissions to the containers in any pod that uses that service account. For more information

As we are using a `Service Account` to allow our Pods to access AWS resources, we make use of the following value.
```
  set {
    name  = "rbac.serviceAccountAnnotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.gitlab_runner_service_account[count.index].arn
  }
``` 

### IAM Roles


### IAM Roles

As we have previously mentioned the Service Account used by the pods allows the pods to gain access to AWS resources. However, we have created a specific Role for the CI Job pod. The Service Account is given an assume role policy with a federated principal of the OIDC provider ARN to allow `AssumeRoleWithWebIdentity`.


```
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${data.aws_ssm_parameter.eks_oidc_token_services[count.index].value}:aud"
      values   = ["sts.amazonaws.com"]
    }

    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.id}:oidc-provider/${data.aws_ssm_parameter.eks_oidc_token_services[count.index].value}"]
      type        = "Federated"
    }
  }


```

It is also provided a policy to allow it to assume a role.

```
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Effect": "Allow",
          "Action": "sts:AssumeRole",
          "Resource": [
              "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/GitlabRunnerExecutorRole"
          ]
      }
  ]
}

```

{{< image src="/blog/static/img/registerRunner.png" alt="group-runner" position="center" style="border-radius: 8px;" >}}